#!/bin/bash
set -e


# Install:
# sudo apt-get install pandoc
# sudo apt-get install texlive-latex-base
#    --csl=iee.csl \

pandoc  text/*.md -o thesis_beiser.pdf \
    --from markdown --template "eisvogel.tex" --listings \
    --toc \
    --csl=ieee.csl \
    --bibliography=ref.bib \
    --toc-depth=4 \
    --number-sections \
    --listings \
    --resource-path=.:assets\
    -V lang=en \
    -H header.tex

pandoc text/*.md -o index.html \
    -t html5 -f markdown \
    --toc \
    --csl=ieee.csl \
    --bibliography=ref.bib \
    --toc-depth 4 \
    --template "github.html5" \
    --number-sections \
    --listings \
    -V lang=de