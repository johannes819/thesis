# Abstract {-}

In recent years, increasingly more software solutions have been developed for the web due to the popularity of web browsers and ease of use. The use of voice assistants has also increased significantly. The main objective of this thesis was to unite these two worlds by using the Speech-to-Text engine DeepSpeech and other freely available open-source components. It will be explained how DeepSpeech works and how the engine can be adapted to individual needs. The decision for the different components will be evaluated on the basis of a prototypical implementation, so that at the end a demo application is available which can transcribe the user's speech inputs in real-time. Client and server-side approaches will be evaluated to allow different application scenarios.

**Keywords:** *Speech recognition, Speech-to-Text, DeepSpeech, Open-source*

# Kurzfassung {-}
 
In vergangenen Jahren werden aufgrund der großen Verbreitung von Webbrowsern immer mehr Softwarelösungen für das Web entwickelt. Auch die Verbreitung von Sprachassistenten hat stark zugenommen. Gegenstand dieser Arbeit soll es sein, diese beiden Welten durch die Verwendung der Speech-to-Text Engine DeepSpeech miteinander zu vereinen. Dabei wird das Ziel verfolgt ausschließlich frei zugängliche open-source Komponenten einzusetzen. Es wird erläutert wie DeepSpeech grundlegend funktioniert und wie man die Engine eigenständig den eigenen Bedürfnissen anpassen kann. Die Wahl der dafür benötigten Komponenten wird anhand einer prototypischen Implementierung auf ihre Eignung und Einsatzmöglichkeiten geprüft, sodass am Ende eine Beispielanwendung bereitsteht welche Spracheingaben des Nutzers in Echtzeit transkribieren kann. Es werden dabei client- und serverseitige Ansätze evaluiert um unterschiedliche Einsatzmöglichkeiten zu ermöglichen.


**Schlagwörter**: *Spracherkennung, spache zu text, DeepSpeech, Open-Source*


\pagebreak