
# Ehrenwörtliche Erklärung {-}

Hiermit versichere ich, Johannes Beiser, ehrenwörtlich, dass ich die vorliegende Bachelorarbeit mit dem Titel: „Analysis and implementation of a web-based Speech-to-Text system with DeepSpeech using freely available software components“ selbstständig und ohne fremde Hilfe verfasst und keine anderen als die angegebenen Hilfsmittel benutzt habe. Die Stellen der Arbeit, die dem Wortlaut oder dem Sinn nach anderen Werken entnommen wurden, sind in jedem Fall unter Angabe der Quelle kenntlich gemacht. Die Arbeit ist noch nicht veröffentlicht oder in anderer Form als Prüfungsleistung vorgelegt worden. Ich habe die Bedeutung der ehrenwörtlichen Versicherung und die prüfungsrechtlichen Folgen (§ 24 Abs. 2 Bachelor-SPO (7 Semester) der HdM) einer unrichtigen oder unvollständigen ehrenwörtlichen Versicherung zur Kenntnis genommen.


Stuttgart, den 12.02.2020

\pagebreak