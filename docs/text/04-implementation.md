# Implementation \label{implementation}
## Introduction demo application

The application in question is a cocktail machine called Mocktail Mixer which was developed by Google in cooperation with the innovation studio Deeplocal as a DIY (do-it-yourself) open source project. All steps to reproduce the hard- and software are being explained in their Github Repository.  

![](assets/images/cocktail_machine.jpg){width=100%}
\begin{figure}[!h]
\caption{Design of the cocktail machine}
\end{figure}

The replica of the cocktail machine is mainly based on the hardware of the "Mocktail machine", but for the replica NestJS^[A node.js framework which builds up on express] is used for the backend instead of Python. Since the machine is to be used at trade fairs, the use of the Google Assistant SDK is not ideal, since it requires an existing connection to the Internet to access the Google Speech API. At this point it makes sense to use DeepSpeech, because then, everything that is technically required for the functionality of the machine is provided within the machine, without any need to communicate with anthing other than it's own locally running web server.  
To start the order by voice there is a button on the side to start and end the recording by holding it. 
In addition to the voice control an app is being developed to create the cocktail via a user interface.

At the start of the recording a Rasberry Pi communicates with the NextJS application which, after processing the voice command, gives the necessary commands to an Arduino microcontroller to pour the right amount of the different drinks into a glass.

### Implementation of the elaborated concepts in a demo application

Since the cocktail machine is still under construction, the concepts worked out will be implemented in a separate application. This application will allow testing the functionality of DeepSpeech beyond the use case of the cocktail machine.

![](assets/images/example_app.png){width=100%}
\begin{figure}[!h]
\caption{User interface of the demo application}
\end{figure}

The demo application supports streaming audio as well as recording and sending a recording. The language model should also be capable of being switched between a custom model that understands various commands specifically for ordering cocktails, and the original language model of DeepSpeech that contains the entire English vocabulary.
Furthermore it should be possible to output the vocabulary via speech synthesis^[Speech synthesis is the artificial production of human speech[@speechsynthesis]] in the browser to use the output of the speaker as input for speech recognition, which simplifies manual testing with a computer generated voice.


## Implementation Frontend

### Access to the user's microphone

Via the MediaDevices interface of the MediaStream API the user's microphone can be accessed via `getUserMedia`. Since the stream is returned as promise, this can be resolved with 'await'.
The `constraints` object can be configured arbitrarily, like in this example with `noiseSupression`, if the device supports this function.


```javascript
let constraints = {
    audio: {
        "noiseSuppression": true
    }
};

let mediaStream = await navigator.mediaDevices.getUserMedia(constraints);
```

If the device doesn't support one of the constraints it's simply not being used.

The stream should be assigned a variable for later use. In this case it contains an audio track, but could also contain a video track if this was configured in the `constraints` object.
The format of the audio stream is 44,100Hz, stereo and 16bit depth in most browsers.

Depending on the browser and version the stream can be output in 16KHz which is possible with the following constraint:

```javascript
let constraints = {
    audio: {
        "sampleRate": 16000
    }
};
```

### Obtaining binary data from the audio stream

Via the MediaRecording API the stream from the MediaStream API can now be further processed with a `MediaRecorder` object. 
When the MediaRecorder is started, it internally collects the audio data as long as it is running.
For each `ondataavailale` event the data collected since the last event can be retrieved. If no interval in milliseconds is specified as a parameter when the MediaRecorder is started, the event is only triggered when the stream ends. If a number is defined, the MediaRecorder triggers an event every 1000ms with the data collected in the last 1000ms, as in the example below. 

The data from the event is output as a BLOB.

```typescript
let mediaRecorder = new MediaRecorder(mediaStream);
mediaRecorder.start(1000);

mediaRecorder.ondataavailable = (e) => {
    let blob = e.data;
    //Send BLOB to Webserver
} 
```

For using the MediaStream API in Angular, it is important to note that there is not yet a MonkeyPatch for zone.js for the API. Zone.js assists Angulars change detection, which is responsible for rendering in the browser when properties change, by monkey-patching the browser's asynchronous API's. Monkey-patching means that a call to the API does not go directly to the API, but first to the wrapper implemented by zone.js, in whose context Angular now detects changes and can re-render if necessary.
Since the MediaStream API provides an asynchronous interface with `oondataavailable` which is not patched, the event handler of the API would have to be executed manually in the zone.js context for use in Angular.


```typescript
// this.zone ist eine injizierte Dependency des
mediaRecorder.ondataavailable = (e) => {
    this.zone.run(() => {
        // Change something that should effect the View
    }
} 
```

The chapter \ref{client_server_communication} describes how the data is transferred to the web server at this point.
If the recording is to be played back in the browser, an `ObjectURL` can be created from the entire `BLOB`, which can be included natively in HTML through the `src` attribute of the `<audio>` tag.

```javascript
window.URL.createObjectURL(blob);
```
```html
<audio [src]="url">
```

If the data should arise in intervals, these chunks of data must be collected cumulatively in a new BLOB, which is possible via the following lines of code.

```javascript
let collectedBlob = new Blob(
    [collectedBLOB, newBlob],
    { type: "audio/webm" }
)
```

### Client side resampling \label{client_resampling}

#### Resampling audio frames
&#8204;\newline
`AudioContext` is an interface that can be used to process audio data.
It can be created via `window.AudioContext` or `window.webkitAudioContext` for Webkit-based browsers.
A data source can be connected with `createMediaStreamSource` as in the following code example.
`createScriptProcessor` creates a `ScriptProcessorNode`, which splits the audio data into 4096 byte buffers and provides them one by one for further processing. This 4096 byte buffer is referred to as an audio frame.
The audio frames are made available in chronological order via the event `onaudioprocess` after the AudioNode is connected to the AudioStream.


```typescript
function resampleInternal(stream){
    const audioContext = new window.AudioContext();
    const audioSource = audioContext.createMediaStreamSource(stream);
    const node = audioContext.createScriptProcessor(4096, 1, 1);
    node.onaudioprocess = (e) => {
        processAudioFrame(e.inputBuffer.getChannelData(0));
    };
    audioSource.connect(node);
    node.connect(audioContext.destination);
    const inputSampleRate = audioSource.context.sampleRate;
}
```

The function `processAudioFrame` accepts an audio frame in any format and converts it to 16KHz.

```typescript
let inputBuffer = [];

function processAudioFrame(inputFrame) {
    for (let i = 0; i < inputFrame.length; i++) {
        inputBuffer.push((inputFrame[i]) * 32767);
    }
    const PV_SAMPLE_RATE = 16000;
    const PV_FRAME_LENGTH = 512;

    while ((inputBuffer.length * PV_SAMPLE_RATE / inputSampleRate) > PV_FRAME_LENGTH) {
        outputFrame = new Int16Array(PV_FRAME_LENGTH);
        let sum = 0;
        let num = 0;
        let outputIndex = 0;
        let inputIndex = 0;

        while (outputIndex < PV_FRAME_LENGTH) {
            sum = 0;
            num = 0;
            while (inputIndex < Math.min(inputBuffer.length, (outputIndex + 1) * inputSampleRate / PV_SAMPLE_RATE)) {
                sum += inputBuffer[inputIndex];
                num++;
                inputIndex++;
            }
            outputFrame[outputIndex] = sum / num;
            outputIndex++;
        }
        
        resampledFrames$.next(outputFrame);
        inputBuffer = inputBuffer.slice(inputIndex);
    }
}
```

The result is output by `resampledFrames$.next(outputFrame)`.  
`resampledFrames$` is an RxJS subject^[Observables are part of RxJs and offer a way to subscribe to new values which are emitted. In contrast to a Promise an Observable can emit multiple values as long as it's not finished. A subject is special kind of Observable that can be called itself to emit a new vaue directly and can multicast to many observers] that outputs each resampled frame in turn.


#### Buffering of the resampled audio frames
&#8204;\newline
Since there is now an output of AudioFrames, an implementation is needed which buffers these frames and outputs an audio buffer or BLOB either in intervals or at the end of the audio stream analog to the fuctionality to the MediaRecorder.

A function is needed that takes the original stream and an interval in which the function should output the resampled data as a parameter. To create a more general solution it is best to return the data in an Int16Array, from which a BLOB can be created later on if desired.
The RxJs subject `resampledFrames$` which is declared globally accessible is instantiated when the `resample` function is called. `resampleInternal` is the function described in the previous chapter which outputs the resampled frames to the subject `resampledFrames$`.

Line 8 is the most important part, which can be described in text as follows:  
For each resampled frame that comes in, buffer it as long as specified in `bufferTime` and return the data reduced in an Int16Array. 
The process is repeated until `streamEndNotifier$` gives a signal. Every interval and at the end `bufferTime$` passes all of the emitted data in an array.
If no interval is specified, the value falls back to the given default. The behavior with an unspecified interval is that the data is collected endlessly until `streamEndNotifier$` ends the observable and `bufferTime` finally returns all data since the start of the recording.

```typescript
let resampledFrames$: Subject<Int16Array> = new Subject<Int16Array>();

function resample(stream: AudioStream, interval = Number.MAX_SAFE_INTEGER ): Observable<Int16Array>{
    resampledFrames$ = new Subject<Int16Array>();
    streamEndNotifier$  = new Subject<void>();
    resampleInternal(stream);

    let resampledStream$ = resampledFrames$.pipe(
        takeUntil(streamEndNotifier$),
        bufferTime(interval),
        map(mergeInt16Arrays),
    );
    return resampledStream$;
}
```

The utility function `mergeInt16Arrays` reduces several Int16Arrays to one. It is needed because the operator function `bufferTime` of RxJs passes the data from the source in an array. Since `resampledFrames$` emits Int16Arrays, this creates an array of Int16Arrays.

```typescript
function mergeInt16Arrays(arrays: Int16Array[]): Int16Array {
    return arrays.reduce((previous, current) => {
        return new Int16Array([...previous, ...current]);
    }, new Int16Array())
}
```

Finally, the functionality presented above can be used as follows:

```typescript
resample(mediaStream,1000).subscribe((data: Int16Array) => {
    //send 'data' each interval to backend
});
```

#### Overview resampling 
&#8204;\newline
The following figure gives an overview of the processing chain of the audio stream.

![](assets/graphics/Architektur_resampling_internal.png){width=100%}
\begin{figure}[!h]
\caption{Overview resampling process}
\end{figure}

The `AudioService` starts the recording by requesting the audio stream from the MediaStream API. This stream is bound to a script processor via the AudioContext Interface of the Web Audio API. The script processor provides the individual frames of the audio stream one after the other for further processing by a resampling script. This resampling script converts the incoming frames to 16KHz and accumulates them internally until the time of the interval specified in the previous example (for example 1000ms) has elapsed or the stream is terminated. These resampled frames are returned as a data chunk in the Int16Array which is emitted in the Observable. The service caller receives the Observable once at initialization and can subscribe to the emitted values.

#### Speech synthesis
&#8204;\newline
Another feature of the application should be to output words via speech synthesis on the speaker. To make this possible the SpeechSynthesis interface of the Web SpeechApi can be used as follows.

```typescript
speakOneWord(word:string){
    let utterance = new SpeechSynthesisUtterance(word);
    window.speechSynthesis.speak(utterance);
}
```

## Client-Server communication  \label{client_server_communication}
### XHR
This chapter will explain the steps necessary to send audio recordings as Int16Array or BLOB to the web server and respond with their transcription.

#### Client
&#8204;\newline
`getTextFromAudio` is a function which is passed the data and returns a promise which resolves when the transcribed text is available.

The function caller can receive the transcription as follows:

```typescript
let transcription = await this.audioService.getTextFromAudio(this.data);
```

Internally, the function generates the HTTP request with the HttpClientModule from Angular. It is to be noted that the request body must contain the data as an `ArrayBuffer` and cannot be sent directly as an `Int16Arrray`. `data.buffer` retrieves the corresponding ArrayBuffer from an Int16Array.


```typescript
function getTextFromAudio(data: Int16Array): Promise<string>{
    return this.http.post<string>("/api/audio",  data.buffer, { responseType: 'text' }).toPromise();
}
```

In comparison, this is how to create the request without a http-wrapper. Angular does the same thing, but hides the implementation with a simpler version.

```typescript
function getTextFromAudio(data: Int16Array): Promise<string>{

    let transcriptions$ = new Subject<string>();

    let xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://localhost:3000/audio', true);

    xhr.onreadystatechange = ()=> {
      if (xhr.readyState == XMLHttpRequest.DONE) {
        transcriptions$.next(xhr.responseText);
        transcriptions$.complete();
      }
    };

    xhr.send(data);
    return transcriptions$.toPromise();
  }
```

It makes sense to put the functionality into an Angular Service like in this example the `audioService`, because it is an interface to the web server and could be used in different places.

#### Server
&#8204;\newline
The previous chapter showed how the client-side connection to the web server looks like when a POST request is sent to `/audio` with the data as a BLOB or Int16Array.

In the backend Node.js is used in conjunction with express.js.
The variable `app` is already available in the following code as an HTTP server created by express.

```javascript
app.post('/audio', (req, res) => {
    let chunks = [];
    req.on('data', (chunk) => {
        chunks.push(chunk);
    });
    req.on('end', () => {
        let buffer = Buffer.concat(chunks);
        let transcription = // get transcription for buffer from DeepSpeech 
        res.send(transcription)
    });
});
```

When the connection is established, an array is created which the asynchronously arriving chunks are temporarily pushed to via HTTP. When the data transfer is complete, the collected chunks are loaded into a buffer, which can be sent to DeepSpeech for further processing and transcription.

One is used to receive data from REST interfaces in the request body. But this is only possible with the use of the `body-parser` middleware. This middleware accumulates the asynchronously arriving chunks into the body equivalent to the code example above. However, the body parser can only handle smaller, less complex data types such as JSON or text. For parsing files like audio data additional middleware like "multer" would be necessary. Since the above implementation works very directly and with little overhead, the use of more complex middleware is not necessary.

### WebSockets \label{websockets}
This chapter will present the steps necessary to stream audio recordings in real time to the server and receive the transcriptions.

#### Client
&#8204;\newline
For the client-side implementation, only the `WebSocket` constructor needs to be used to connect to the URL of the WebSocket server.
The `onmessage` event is used to receive messages from the web server.
The WebSocket connection should only be held up as long as the audio recording is streamed. To avoid sending data to the web server while the connection is not established, it makes sense to put the establishment of the connection into a function which returns a promise which is resolved as soon as the connection is established.


```typescript
let ws: WebSocket;
function websocketConnection(): Promise<void> {
    let isOpen = new Subject<void>();

    ws = new WebSocket("ws://localhost:3000");
    ws.onopen = () => {
        console.info("Connection is open");
        isOpen.complete();
    };

    ws.onmessage = (e) =>{
        console.info('Received message: ${e.data}');
        //e.data contains message
    } 

    ws.onclose = () => console.info("Websocket connection is closed");

    return isOpen.toPromise();
}
```

Now, to send data to the WebSocket server the establishment of the connection can be awaited before sending it.

```typescript
await websocketConnection();
this.ws.send(data);
```

It is therefore necessary to make the WebSocket connection accessible via a global variable.
In the case of the demo application , it is waited for the connection to be established when the user starts recording before registering the media recorder's `ondataavailable` event.
Once the connection is established and the `ondataavailable` event is registered, the data chunks received from the MediaRecorder can be transferred in the event handler using `ws.send()` easily.

#### WebSocket Server
&#8204;\newline
After creating the web server, a WebSocket server is required for the WebSocket connection.
It can be easily created and provides the two implementable event handlers `message` and `close`.

```javascript
let app = express();
const server = http.createServer(app);

const websocketServer = new WebSocket.Server({ server });
websocketServer.on('connection', (ws) => {    
    ws.on('message', (data)=>{
        //handle data
    });
    
    ws.on('close', () => {
        //handle closed connection
    })
});
```

If a data chunk sent by the frontend is received, it is directly available in the `message` event as `data` as a buffer as in this example.

\pagebreak
### Connection establishment

The following figure shows the timing of the connection establishment via WebSockets which is established when audio streaming is started by the user.

![](assets/graphics/Websocket_Timing.png){width=100%}
\begin{figure}[!h]
\caption{Timing of connection establishment for streaming with WebSockets}
\end{figure}

This approach transcribes the data only after the WebSocket connection is established. If the connection takes longer to establish, words previously recorded are ignored. The problem can be solved in the implementation by starting the media recorder in parallel to the connection. In this case it may happen that an `ondataavailable` event occurs when the connection has not yet been established. Therefore it is necessary to check in each of these events whether the connection is established and if not, to buffer the data in the background until the connection is established. In the `onopen` event of the WebSocket the accumulated buffer can then be sent initially.

In addition, when the connection is terminated, the recording is terminated without taking into account the last interval still running. In the worst case with an interval of 1000ms a time span of 999ms is lost which is not transcribed.  

The fundamental problem is that the connection may only be terminated when the last transcription has been sent.
To make this possible the following steps are necessary:  

- In each `ondataavailable` event, before sending the data, it must be determined if it is the last event to come, which is possible by setting a boolean flag when the recording is stopped.
- If this is the case, in addition to the audio data, the WebSocket server must be told that this is the last chunk to come.
- The server must remember this and also signal in the response with the last transcription that this is the last transcription, which is another challenge, as it can happen that within the last data chunk two separate transcriptions are created by the `VAD` component and sent one after the other.
- Only after the client has received the last transcription it can terminate the connection from its end.

For reasons of complexity, however, both optimizations will be omitted at this point.  
The problem during connection establishment can be made clear to the user in the user interface by displaying the connection status.  
Due to its use cases, audio-streaming is more likely to be used in such a way that it is started once, then runs for a longer period of time and is only terminated when the application is closed, for example.  
For these reasons the loss is within reasonable limits.  
For the simplified case of transcribing a completed recording, these problems are either not relevant as described in the chapter \ref{client_resampling}, or have been taken into account in the implementation.

For larger intervals the effect would be correspondingly worse, but it is not advisable to select a larger interval than 1000ms if the transcribed text is to be output in real time as in the specific case of the demo application.
If the audio stream is only to be transcribed in the background - for example for transcribing a speech or a telephone conference - it would also be sufficient to select an interval of 30 seconds. In this case the optimizations described above would be advisable.


## Implementing the web servers
### General setup

For the fundamental setup of the backend, a simple HTTP server with exress and a WebsocketServer is created. 

With `app.use`, the necessary middleware can be used. `express.json` and `express.urlencoded` are both configurations for the middleware `body-parser` which writes the data arriving asynchronously via HTTP into the request-body.

```javascript
const express = require('express');
const http= require('http');

let app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));

const server = http.createServer(app);
const websocketServer = new WebSocket.Server({ server });

server.listen(3000, function () {
    console.log('Webserver listening on port 3000!');
});
```

### Pipelining and transcoding the audio stream \label{pipelining}

Both with XHR and when sending data via WebSockets, the data is available as a buffer.

```javascript
req.on('end', () => {
    let buffer = Buffer.concat(chunks);
});

ws.on('message', (data)=>{
    //handle data
});
```
\pagebreak
With XHR the buffer is already given in the required format as s16le PCM, so it can be transcribed directly. For streaming over WebSockets, however, the pipeline evaluated in the chapter \ref{evaluation_pipeline} must be implemented.

#### Creating a ReadStreams
&#8204;\newline
If the buffers are coming in in 1000ms intervals from the frontend via the WebSocket connection, they can easily be pushed into a `Readable` stream. This stream is needed as the source of the pipeline.


```javascript
audioStream = new Readable({
    // The read logic is omitted since the data is pushed to the socket
    // outside of the script's control. However, the read() function 
    // must be defined.
    read(){}
});

ws.on('message', (data)=>{
    audioStream.push(data);
});
```

If `null` is pushed into the readable stream the EOF flag (End of File) is set which causes the stream to end.

#### FFMPEG
&#8204;\newline
After the audio stream is available on the server side, it must be resampled.
An FFMPEG command can be easily executed in node in a child process with parameters. 

The following parameters are the most relevant ones:

- &#8204; `-i` indicates which file should be transcoded. If `-` is given, ffmpeg expects the data to be loaded as a stream via the interface `ffmpeg.stdin`.
- &#8204; `-acodec` specifies the audio codec for the output.
- &#8204; `-ar` specifies the sample rate for the output.
- &#8204; `pipe:1` specifies that the transcoded data should be output as a stream on `ffmpeg.stdout`.


```javascript
    ffmpeg = spawn('ffmpeg', [
        '-hide_banner',
        '-nostats',
        '-i', '-',
        '-vn',
        '-acodec', 'pcm_s16le',
        '-ac', 1,
        '-ar', AUDIO_SAMPLE_RATE,
        'pipe:1'
    ]);
    audioStream.pipe(ffmpeg.stdin);
```

The resampled stream is available on FFMPEG's streaming output `fmpeg.stdout`.

#### VAD
&#8204;\newline
To use Voice Activity Detection (VAD), only the NPM package must be integrated.
When generating the stream it is necessary to specify the format of the incoming data. 
The `mode` can be used to specify how sensitive or aggressive VAD should be to trigger activation. 
Different configurations are recommended for different applications:

- NORMAL: Detection mode with lowest miss-rate. Works well for most inputs.
- LOW_BITRATE: Optimized for low bitrate recordings.
- AGRESSIVE: Recommended for environments with some background noise or low quality recordings.
- VERY_AGRESSIVE: Suitable for high bitrate, low-noise data. May classify noise as voice, too.

Depending on the choice of this configuration, even quiet background noise may activate the stream.

The `debounceTime` specifies how many ms of silence must elapse before the voice input is recognized as finished.


```javascript
const VAD = require("node-vad");

const VAD_STREAM = VAD.createStream({
    mode: VAD.Mode.Normal,
    audioFrequency: 16000,
    debounceTime: 20
});

ffmpeg.stdout.pipe(VAD_STREAM);
```

The generated `VAD_STREAM` is an extension of the Transform Stream - a special duplex stream from Node.js. Each output of the VAD stream contains a buffer with the audio data that VAD recognized as speech input.

This data can be received with `VAD_STREAM.on('data', handleData)`, whereas `handleData` is the function that receives the buffer and sends it to DeepSpeech for transcription.

### Integrating DeepSpeech in Node.js

For using DeepSpeech in Node.js, four things are generally required:

- NPM package `DeepSpeech
-  Acoustic model
- "trie" and "lm.binary" file for the language model

All of the required files are already provided by DeepSpeech. The chapter \ref{create_am} and \ref{create_lm} will explain how to create and modify them independently.

```javascript
const Ds = require('DeepSpeech');

const LM_WEIGHT = 0.75;
const BEAM_WIDTH = 500;
const LM_BETA = 1.85;

const model = new Ds.Model('/models/output_graph.pbmm', BEAM_WIDTH);
model.enableDecoderWithLM('/models/lm.binary','/models/trie', LM_WEIGHT, LM_BETA);
```

In the last two lines of code the model is created and linked to the Language Model. Some very technical parameters must be specified.
The only parameter of interest to the developer is `LM_WEIGHT` which specifies how much the language model may affect the transcription. The value is to be understood as a percentage (0.75 = 75%). 
The two parameters `BEAM_WIDTH` and `LM-BETA` are necessary for the configuration of CTC and will not be explained further, as they go very deep into the implementation of the decoder and will be hidden by the API in future versions of DeepSpeech [@decoderApiChange].


### Transcribing the buffer with DeepSpeech
The chapter \ref{pipelining} explained how to prepare the audio stream for processing for DeepSpeech. This chapter shows how to transfer the resulting resampled buffer to DeepSpeech.


#### Basic transcription of a buffer from a finished recording
&#8204;\newline
By calling `model.stt(buffer)` the transcribed string of the buffer is returned.
Currently, a small workaround is necessary due to the underlying implementation of DeepSpeech, so only half of the `buffer` must be provided.
The reason for this is that the Node.js Buffer API does not allow to specify the size of each element contained in the buffer and sets it to 8 bit. However, since one sample of the audio signal is 16 bit in size, one sample must be stored in 2 elements of the buffer which takes up 16 bit. 
When reading the buffer, DeepSpeech first determines how many elements it contains and assumes that one element corresponds to one sample. Then the number of elements in the buffer is read in 16 bit steps, starting from the memory address of the buffer's start point. 

This would result in reading beyond the memory of the last element from the buffer, because when 64.000 elements which are 8 bit each are being read in 16 bit chunks (64.000 * 16 bit) there won't be any data after 32.000 * 16 bit in memory. To avoid this problem it is currently only possible to transfer half of the buffer. Thus DeepSpeech reads in only 32.000 * 16 bit, which is the same length as the original 64.000 * 8 bit buffer. This workaround only works because `Array.slice` returns an object that has a different start- and end pointer, but shares the underlying memory with the original buffer. The cause of this problem is the mapping from a Node.js buffer to a python buffer, which internally has to rely on reading in the data from the memory in order to convert it.


```javascript
let result = model.stt(buffer.slice(0, buffer.length / 2));
```

Besides simply obtaining the string with the transcription, it is possible to get additional information about the transcription with `model.sttWithMetadata()`. This includes the `confidence` of the transcription which indicates how sure the acoustic model is about the result and a list of letters contained in the transcription together with their timestamp from the recording. The value of the confidence is the natural logarithm of the probability. In order to determine the actual probability value, it must be converted, whereas `l` corresponds to the logarithmic value "logit":

\begin{equation*}
P = \frac{e^l}{1+ e^l}
\end{equation*}


It should be noted, however, that the transcribed text must be put together by hand, as only the letters are given individually without including the entire transcription. Also currently, the confidence for correct transcriptions can often be around just 10% because the amount of alternatives are too high. 

If it would be an array of letters it could be easily merged with `Array.join()`. But since every `MetadataItem` is an object containing the letter and the corresponding timing information, the array must be reduced as follows:

```javascript
const metadataResult = model.stt(buffer.slice(0, buffer.length / 2));

const transcription = metadataResult.items.reduce((acc, curr)=>{
    return acc + curr.character
}, "");

const probability = Math.exp(metadataResult.confidence) / (1+ Math.exp(metadataResult.confidence));
```

#### Streaming in DeepSpeech
&#8204;\newline
To stream data to DeepSpeech the stream is created as follows.

```javascript
let stream = model.createStream();
```

Then data chunks can be loaded into the stream.

```javascript
model.feedAudioContent(stream, chunk.slice(0, chunk.length / 2));
```

At this point, DeepSpeech does not respond automatically with the transcriptions, but processes them internally. 
While the stream is active, a temporary transcription can be received as follows:

```javascript
let result = model.intermediateDecode(sctx);
```

At the end of the stream the final transcription can be obtained with `let result = model.finishStream(stream)`.
Even if `intermediateDecode` is not called while the stream is active, `finishStream` will return the result without delay at the end.

In addition, there is also the variant `model.finishStreamWithMetadata()` which is equivalent to `model.sttWithMetadata()`, but this does not work with `intermediateDecode`.


### Creating a custom language model \label{create_lm}

To create a custom Language Model a program called "kenlm" is needed first. This can either be cloned from Github and built independently to get executable scripts, or be downloaded as executable binaries for the appropriate platform. 
Kenlm creates the file `lm.binary` which DeepSpeech needs directly from a text file `vocabulary.txt`.
To get the second file `trie` DeepSpeech provides a program with the "Native_Client" which creates it from `lm.binary` and an `alphabet.txt`.
The vocabulary in `vocabulary.txt` contains all words that should be recognized. It may not only contain single vocabularies but phrases as well. Each entry, whether word or phrase, goes into a separate line. Words may therefore appear several times in combination with other phrases.
None of the words may contain a capital letter, as kenlm cannot handle this. Special characters like `'` are allowed.

The following directory structure is recommended for the creation the language model:

```
utils
|   generateLM.sh
| 
|---training_material
|   |   alphabet.txt
|   |   vocabulary.txt
|
|---output_files
|   |   ...
|
|---kenlm
|   |   ...
|   
|---native_client
|   |   ...
|   
```


If all required programs and files are provided, `generateLM.sh` can be executed with the following commands

```shell
./kenlm/build/bin/lmplz --text training_material/vocabulary.txt --arpa output_files/words.arpa --order 5 --discount_fallback --temp_prefix /tmp/
./kenlm/build/bin/build_binary -T -s trie output_files/words.arpa output_files/lm.binary
./native_client.amd64.cpu.osx/generate_trie training_material/alphabet.txt output_files/lm.binary output_files/trie
```

After executing this script the files `words.arpa`, `lm.binary` and `trie` are located in the directory `output_files`, of which only the last two are needed by DeepSpeech.


## DeepSpeech client-only
In previous chapters, comments have been made regarding client side usage. This chapter is intended to explain what would be necessary for this in the future.

On the client side, DeepSpeech can only be used as a native client on the various operating systems, for example for a Windows App. For the Web, there is currently no way to run DeepSpeech purely in the user's browser. Although the necessary models have been scaled down considerably in version 0.6.0, there is still no possibility to run DeepSpeech only in the browser. Since version 0.6.0, DeepSpeech also provides the pre-trained model compressed in .tflite format. `tflite` or "TensorFlow Lite" optimizes the models created with TensorFlow especially for mobile devices by using a special memory strategy and by excluding special and rather rarely used operations. For example, a model optimized with TensorFlow Lite has only about 40 MB instead of 200 MB in the case of DeepSpeech. A complete and comprehensive language model still is 1000 to 2000 MB in size, whereas a minimal language model with just 20 words is only about 1 KB. 

Since all preceding steps such as the resampling of the stream are possible to be performed on the client-side, only the integration of DeepSpeech on the client side is missing, which is why a web server is currently still needed.


\pagebreak