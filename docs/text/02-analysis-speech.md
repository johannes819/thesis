# Analysis and technical foundations for speech recognition systems 
 \label{analysis_stt}

This chapter will analyze which technologies are suitable to ensure robust^[speech recognition capable of working very well in various kinds of environments] speech recognition. In particular explaining the individual components of a speech recognition system and how they interact with each other to make the decisions for the different options clearer.

## Requirements and comparison between existing technologies	

### Requirements for a Speech-to-Text engine

#### Classification
&#8204;\newline
Probably the most basic requirement of a speech recognition system is to be able to distinguish between different words. This can be important if it only needs to recognize words within a domain of words, for example the recognition of control commands for a game ("up", "down", "left", "right").

#### Transcribing Audio
&#8204;\newline
More complex speech recognition systems are expected to convert the speech directly into text. In terms of complexity, this differs from simply transcribing already fully captured recordings in clear spoken language to transcribing difficult to understand accents and dialects with background noise and poor SnR (signal-to-noise-ratio). In addition, an advanced STT engine should be able to process a continuous stream of audio in real-time. During speech, the recognized words should already be transcribed and output by the STT engine.

### Comparison of the existing speech recognition technologies
#### Google Cloud Speech API
&#8204;\newline
Even though the Google Cloud Speech API is not an option for this thesis because it is a paid API which is not owned by the open-source community but a product of Google, this option is still listed because it is a very popular and widely used way to convert speech to text on the web.

#### TensorFlow.js
&#8204;\newline
Tensorflow.js is a machine learning library for javascript. It is a javascript based implementation of the TensorFlow framework from Google for Python. It can be used "[...] for training and deploying machine learning models in the browser and in Node.js" [@tensorflowjs]. If trained to create a speech recognition model it is able to classify words.
To create a classification model the web interface of the project "TeachableMachine" can be used by speaking samples of words which the model should recognize [@teachablemachine].
This model has only a few megabytes and can therefore be provided on the client side which has the great advantage that it can also be used offline.  
The figure shows an demo project which can distinguish between background noise and the words "Hello" and "World".


![](assets/images/tensorflow_js.png){width=85%}
\begin{figure}[!h]
\caption{TeachableMachine demo project}
\label{teachablemachineFigure}
\end{figure}

Tensorflow.js is a low level solution with which neural networks can be trained easily and quickly. For more complex applications it is not suitable due to the problems mentioned in the following section.


At the moment it is only possible to recognize words with a duration shorter than one second. Using the web interface from figure \ref{teachablemachineFigure}, samples of the sound/word must be recorded. Once a sufficient number of recordings have been obtained, the model can be trained. The model may be sufficient for a particular case, but its great weakness is that it is exactly adapted to the pronunciation of the training material. There is no corpora - a set of recordings - that specifically consists of recordings of individual words, so it is necessary that all words are recorded for training the model itself. For a robust model, it would not be sufficient to provide only the minimum number of eight example recordings [@tensorflowjs].
Furthermore, the model is only able to compare how similar an input sounds to one of the possible outputs without having an understanding of letters or words. Therefore Tensorflow.js cannot be used for transcribing speech, just very basic classification. 

If the machine-learning model should be able to understand new words it is necessary to train it further, which is a disadvantage compared to a more sophisticated system where it is often sufficient to enter the new vocabularies as a string, because the model already has a fundamental understanding about the pronunciation of letters and words.

Another problem is the task of classification itself. Ideally, the end user should only speak words that can be understood by the model. If a unknown word is spoken, the most similar sounding word is taken instead of ignoring it. Ideally, a model should be based on all words of a language and output only when certain words are recognized.


#### DeepSpeech  
&#8204;\newline
DeepSpeech is an open-source implementation by Mozilla based on the DeepSpeech speech recognition architecture introduced by the company Baidu. It is to be distinguished between the implementation "DeepSpeech" by Mozilla and the architecture "DeepSpeech" by Baidu. There are also many other implementations of this Architecture, though Mozillas implementation is the most sophisticated one [@deepspeech_implementations]. Mycroft for example, a company developing an open-source voice assistent has switched to using DeepSpeech internally [@mycroft].  
Although the implementation of Mozilla follows the DeepSpeech architecture quite closely, it deviates in some places. In addition, Baidu has meanwhile published another paper called "Deep Speech 2 : End-to-End Speech Recognition in English and Mandarin" , which also plays into the implementation of Mozilla in parts [@deepspeech_2]. In the further course of this paper "DeepSpeech" refers to the implementation of Mozilla. In the research paper "Deep Speech: Scaling up end-to-end speech recognition", published in 2014 by Baidu, an architecture is presented that allows to use a single artificial neural network to directly infer^[Inference is the process of making prediction based on a artificial neural network] text from an audio file. Why this is beneficial and what exactly neural networs are, will be explained in chapter \ref{ann}.  

In the end of 2017 Mozilla released the first version 0.1.0 of this implementation. Currently, DeepSpeech is in version 0.6.0 which is still a very early stage of development. DeepSpeech supports both fixed-length audio transcription and an audio-stream interface that outputs the transcribed text of an audio stream in real time. DeepSpeech is implemented with TensorFlow, a machine learning framework from Google in Python. 

The main idea behind DeepSpeech is to create a simple, yet effective Speech-to-Text engine that does not require expensive server hardware and runs efficiently even on devices with limited hardware resources. This is to be made available on different platforms and for various languages free of charge under the "Mozilla Public License" [@deepspeech_docs].

In 2017, Mozilla also started the crowdsourcing project CommonVoice in parallel to DeepSpeech in order to create a constantly growing corpus for DeepSpeech. Corpora are datasets containing speech and the corresponding transcription and are mainly being used for the training of automatic speech recognition (ASR) Systems. A further analysis of Corpora will be made in chapter \ref{corpora}.

![](assets/images/DeepSpeech_overview.jpeg){width=85%}
\begin{figure}[!h]
\caption{DeepSpeech and CommonVoice}
\label{DeepSpeechOverview}
\end{figure}


This means that DeepSpeech can be used to transcribe any language as long as the training data is available for that language to train a model with. As an example, DeepSpeech is already being used to develop a voice assistent for Maori [@deepspeech_maori].

DeepSpeech is available in several programming languages including Node.js. In future versions it is planned to be made available for the browser on the client side. On various platforms such as Windows, MacOS or Rasberry Pi, DeepSpeech is already available as a native client, but for web applications a web server is still required to run DeepSpeech.


#### Kaldi
&#8204;\newline
Kaldi is a open-source toolkit for speech recognition targeted for researchers. It can be used to train speech recognition models and to decode audio [@kaldi_medium].
While there are a several pretrained models provided for different languages, Kaldi is mostly targeted for developers who want to create or improve their own model, with their own or another publicly available set of data.
It is written in C++ and generally more complex and difficult to use compared to DeepSpeech, since it doesn't offer language-bindings for other programming languages [@kaldi_asr].
However it is currently still the Speech-to-Text engine of choice for many companies who work in the field of open-source speech recognition since it was continuously improved since it was released in 2009 [@kaldi_companies] [@ravanelli2018pytorchkaldi]. In contrast to DeepSpeech developers can create different models and STT architectures with it. This is why Kaldi is more to be seen as a actual toolkit rather than a ready-to-use Speech-to-Text engine like DeepSpeech.


#### Evaluating the effectiveness of a speech recognition system
&#8204;\newline
The accuracy of a speech recognition system is measured by the word error rate (WER). The goal of any speech recognition system is to get close to the WER of a human. Contrary to the expectation this is not 0% but about 5.8% [@deepspeech_2]. 
To compare the different speech recognition systems there are tests based on different corpora which measure the WER. A corpus is the set of data a speech recognition system is trained on. For testing purposes it is possible to test a speech recognition system on a different set which it was not trained on. These tests result in different WER depending on the test set.
Only at a deviation in WER around 5-10% a person will notice a quality gain or loss [@wer_humans]. There are numerous comparisons between word error rates among the different speech recognition systems and it does not necessarily mean that one is superior to the other if it achieves a better result for one test set. For competitive reasons each system tries to present itself better than the other, which is achieved by choosing a special test corpus. This is the reason why many reported word terror rates are often quite far from a real life scenario and lack meaningfulness, as the value simply states that this speech recognition system is very good at recognizing a special accent in a special scenario with a limited list of words known to the system [@wer_comparison]. In individual cases, such tests can make sense if the application area is isolated and consistent. To be able to draw a real conclusion one has to look at the WER of different test sets.
The following table compares the results of different tests on the DeepSpeech 2 implementation of Baidu with the WER of a human for each test. The values of human WER are measured and provided by the respective test sets. The DeepSpeech implementation of Mozilla has only been measured by one test (LibriSpeech - clean) at 7.5%, which is very close to the WER of Baidu's implementation, therefore the measured values of Baidu are shown below as an example [@deepspeech_2].


\begin{table}[h!]
\begin{tabular}{|c c c c|} 
\hline
\textbf{Category}    & \textbf{Test Set}       & \textbf{DeepSpeech by Baidu} & \textbf{Human WER} \\ [0.5ex] 
\hline
Clean Read  & WSJ eval’92    & 3.10 & 5.03 \\ 
Clean Read  & WSJ eval’93    & 4.42 & 8.08 \\ 
Clean Read  & LibriSpeech test-clean & 5.15  & 5.83 \\
Clean Read  & LibriSpeech test-other & 12.73 & 12.69 \\
\hline
Accented    & VoxForge American-Canadian & 7.94 & 4.85 \\
Accented    & VoxForge Commonwealth & 14.85 & 8.15 \\
Accented    & VoxForge European & 18.44 & 12.76 \\
Accented    & VoxForge Indian & 22.89 & 22.15 \\
\hline
Noisy       & CHiME eval real & 21.59 & 11.84 \\
Noisy       & CHiME eval sim & 42.55 & 31.33 \\

\hline
\end{tabular}
\caption{Word error rate of DeepSpeech}
\label{WER of different test-sets for DeepSpeech compared to humans}
\end{table}


In the following table, the different speech recognition systems are compared with the corresponding WER as an example. The tests were performed with an internal test set from Baidu, which is a combination of several test sets. 

\bigskip

\begin{table}[h!]
\begin{tabular}{|c c c c|} 
\hline
System              & Clean & Noisy & Combined \\ [0.5ex] 
\hline
Apple Dictation     & 14.24 & 43.76 & 26.73 \\ 
Bing Speech         & 11.73 & 36.12 & 22.05 \\
Google API          & 6.64  & 30.47 & 16.72 \\
wit.ai              & 7.94  & 35.06 & 19.41 \\
DeepSpeech (Baidu) &  \textbf{6.56}  & \textbf{19.06} & \textbf{11.85} \\ [1ex] 
\hline
\end{tabular}
\caption{Word error rate of different ASR-Systems}
\label{WER comparison between ASR-Systems}
\end{table}


### Corpora for training a speech recognition model \label{corpora}
A corpus (plural corpora) is a collection of audio files linked to the corresponding text it contains. They can be used to train ASR models since they provide both the audio and the corresponding transcription. 

There are several different corpora which are publicly available. The choice of audio data used to train the ASR model has a direct impact on how robust the system will be and what language variations it can understand [@corpora_general]. For example, if one uses only clearly spoken English without background noise, the resulting model will recognize clear English very well, but will not be able to handle accents or background noise well.
Also, the different corpora have different amounts of voice data. Some systems consist of over 1000 hours of speech, while others consist of only 100 or 200 hours. 
To train a sufficiently robust system, according to Baidu about 10.000 hours are required [@deepspeech_2].
Where this data comes from does not matter as long as it is all in the same format and some other basic requirements are given.

When choosing the corpus / corpora, one should therefore make sure that it is adapted to the scope of the ASR system. Ideally it should be a corpus containing data with bad signal to noise ration (SNR) and strong accents to cover every case in a real world scenario.
In the further course of this chapter an analysis will be made to what extent the currently existing corpora are suitable to train a robust an adapted speech recognition system.

#### LibriSpeech
&#8204;\newline
LibriSpeech is the corpus that emerged from the LibriVox project. LibriVox is a collection of freely available audio books in mainly plain American English without background noise. LibriSpeech contains about 1000 hours of speech and is currently one of the corpora used for DeepSpeech . LibriSpeech is not suitable as the sole source for training a robust speech recognition because the corpus contains only noise-free and clear speech.

#### CommonVoice
&#8204;\newline
To solve the problems mentioned in previous chapters, Mozilla started the CommonVoice project in june 2018. The corpus is currently still under construction and unsuitable as the sole source for training a speech recognition system since it doesn't yet contain enough data. CommonVoice is a crowdsourcing project where one can contribute to the corpus by recording one' s voice. The web user interface has two modes - speaking and listening. When speaking, a short sentence oaf about 3-10 seconds is given which is then recorded. Once five of these sentences have been recorded, the recordings are shared with other users for validation. These recordings are then validated by other users of the platform who have selected the "listen" mode. One listens to 5 random recordings of other users and then indicates whether they match the given sentence. The quality of the recording does not matter as long as one understands each of the spoken words. If a recording has been validated by two different users, it is included in the data set. If the recording is rejected by at least two users, the recording is included in the so-called "Clip Graveyard" which is also available for download.

The platform tries to motivate users to be active by methods like gamification^[Gamification commonly employs game design elements to improve user engagement in a non-game context]. For example, one can set daily or weekly goals to achieve a certain number of recordings or validations and is also placed in a worldwide raking depending on how many of one's own recordings have been validated.
The result of this platform is a corpus consisting of different accents and background noise levels. It should also be noted that the project not only collects English but also offers a lot of other languages due to the way it works. English currently is the most actively contributed language with about 1000 validated hours. thesis In total of all languages, about 2500 hours have been validated.

It is important to note that of the 1000 hours only 88 hours are unique. The reason for this is that until the beginning of 2019 there was only a very small amount of sentences, resulting in duplication, which is not optimal for training. In early 2019 Mozilla started the project SentenceCollector [@sentenceCollector] which works after the same principle as CommonVoice to collect sentences from everyday language. In addition to this, 1.000.000 sentences were extracted from Wikipedia articles, so CommonVoice now has enough sentences to avoid duplicate recordings in the future.
Criticism regarding this would be that the complexity of the sentences extracted from Wikipedia to be spoken exclude some users such as children or people who have difficulties with the language. Since the aim of CommonVoice is capturing the "common voice", the voice of everyday speech and speakers, this could be counterproductive.

Additional metadata such as the speaker's origin is stored with the respective recordings, if the speaker has registered before. This way it would even be possible to filter the data of a special accent in order to develop a speech recognition model for an exact target group or specific accent.


#### Selecting the corpus
&#8204;\newline
The different corpora were introduced in order to have an understanding on how they can be different. There are many other corpora, including TED-LIUM, SWITCHBOARD and Fisher Corpus.  
The choice of the suitable corpora for training a speech recognition system should be based on the target users for that speech recognition system. If one wants a quite specific system for a special accent of a language, it makes sense to adapt the training material to it. On the other hand, if one wants to use it for  a wide range of speakers the ASR should be able to handle any kind of background noise, different environments and accents. In such a scenario it makes sense to use multiple corpora or even all corpora available in that language. CommonVoice would be the most suitable corpus in this case, although there is no reason not to use other corpora as well.


## Technical foundations for DeepSpeech

This chapter explains the basics necessary to understand DeepSpeech and introduces some technical terms.

### Artificial neural networks \label{ann}
Artificial neural networks (ANN) are based on the model of neurons in the nervous system of a human brain, although the primary objective is focused on creating models for information processing purposes rather than reproducing the actual biological structures. This chapter will describe a few types of neural networks that exist, how they function fundamentally and how they are trained.

#### Abstract functionality of a neural network
&#8204;\newline
The basic task of an artificial neural network (ANN) is to conclude the respective output on the basis of an input. It consists of a set of artificial neurons which are connected to each other on different layers [@ann_ref].
There is always an input and output layer. In between there are one or more so-called "hidden layers" which are responsible for the conclusion of the input. One layer of a neural network is responsible for one task. In the example of a ANN for image recognition, one layer could for example be responsible for transmitting a signal depending on the brightness, which is then used as input by the next layer of neuron. The topology of the network depends on the exact task of the neural network. The topology describes the structure of the network which consists of the number of layers, neurons and the weighted connections of the neurons among each other. The connections between the neurons can have different weightings which determine how much influence one neuron has on the result. The output can be of any format. It could be a simple binary classification (right or wrong) or more complex output formats such as letters.

The following figure shows such an exemplary topology.

![](assets/images/knn.png){width=65%}
\begin{figure}[!h]
\caption{Topology of an artificial neuronal network}
\end{figure}


#### Recurrent neural network
&#8204;\newline
The recurrent neural network(RNN) is a type of ANN. Compared to a simple neural network as shown in Figure 1, it differs in that the output of a neuron of a higher layer can be used as input of a neuron of a lower layer. This feedback makes sense for example if the input of the ANN changes over time [@fujita1994evolutionary]. An example would be a video stream as input of a ANN in which the result of a neuron at time $t_1$ is used as input of a neuron at time $t_2$. If the input of a neural network is changing over time, the topology of the network must determine which duration is used as a single input for the network. For the processing of a video stream, for example, one frame could be used. 

![](assets/graphics/rnn.png){width=65%}
\begin{figure}[!h]
\caption{Topology of a recurrent neural network}
\label{rnn_image}
\end{figure}

The figure shows how the recurrent layer passes on information from the previous to the following iteration.

#### Supervised learning
&#8204;\newline
Supervised learning is understood to be the training of a neural network with samples of input data and their corresponding results [@chao2020revisiting]. Using speech recognition as an example, one would feed pairs of speech recordings with their transcription into the neural network. The network first automatically infers the output value with the current neural network topology and compares this actual state with the target state which is given in the training data. Depending on the deviation from the actual to the target state, changes are made to the network topology and the next iteration starts.

#### End-to-End automatic speech recognition
&#8204;\newline
Traditional speech recognition systems consist of different components such as the acoustic model, language model and a pronunciation model. These components are independently developed models and can be implemented differently depending on the architecture of the speech recognition system. In order to conclude an output, the input from the individual modules must be analyzed one after the other [@e2e_asr].
For example, so-called phonemes are used in traditional speech recognition pipelines. A phoneme is a written representation of the corresponding pronunciation of a word. The acoustic model learns which phonemes are associated with a sound based on carefully entered training data with aligned words (words where the pronunciation of each letter of the word is aligned in time). Since it knows from the training data which sound is found at which position of the word, it is easy for the model to output a phoneme representation of the word. This output must then be passed further into the speech recognition pipeline and processed by the next model which for example converts a phoneme representation of a word into letters.  
The development of such an architecture is very complex and every single model has its own problems. Each of the models must be optimized and trained independently, which can quickly lead to a bottleneck when used together.
With End-to-End (E2E) ASR the goal is to limit these components to a single model so that the final output can be deduced directly from the input [@e2e_asr]. 
The advantage of E2E ASR is that only a single model needs to be trained, the output is the transcribed result, and no aligned recordings are needed for training in order to learn how to pronounce individual letters. This model is still called the "acoustic model" and will be referred to as such in the further course of this thesis.

## The DeepSpeech architecture based on Mozilla
After providing some basic knowledge about speech recognition in the previous chapters, this section will explain how DeepSpeech works. The implementation of Mozilla follows Baidu's original idea but it differs in some ways [@deepspeech_docs_official].

### Abstract architecture  
DeepSpeech uses a E2E recurrent neural network to infer the transcribed text directly from the audio input.  
In the first step, the raw audio signal is converted into a suitable data format which is then analyzed by an acoustic model in sequences of 20ms, so that for each of these 20ms frames a probability for the letter is  given. A decoder then calculates the most likely resulting word and word sequences from all the frames in conjunction with a language model.

### Required input format 

So called features are needed for further processing for the acoustic model. These features serve as input values for the RNN and are a mapping of the raw audio data to a specific data structure. As an input format, DeepSpeech produces a Mel Frequency Cepstral Coefficients (MFCC) from the raw PCM^[Pulse-code modulation - the standard form of digital audio. The analog signal is sampled regularly at uniform intervals in order to oblain a uniform digital signal [@pcm]] audio signal, which is a type of spectrogram - a compact representation of the frequency spectrum of the audio signal. This allows DeepSpeech to make decisions with the neural network based on a few absolute values.

### The acoustic model of DeepSpeech

The acoustic model of DeepSpeech basically consists of two parts. The RNN which outputs the probabilities of the individual letters and a beam search decoder which decides on the most probable result based on these matrices.


#### Structure & task of the RNN
&#8204;\newline
After the MFCC is created the RNN iterates in 20ms frames over the entire length of the data or continuously over a stream. For each of these 20ms sequences a probability matrix is created which contains the probabilities of the possible symbols, for example the english alphabet. A decoder calculates the most probable sequence of symbols to determine the final word. 

This described process is performed within the acoustic model so it can be used on its own to convert speech to text. Additionally, a language model can be used to gain further accuracy [@deepspeech_docs_official].

![](assets/images/deepspeech_rnn.png){width=85%}
\begin{figure}[!h]
\caption{Topology of the RNN of DeepSpeech} \caption*{Source: https://deepspeech.readthedocs.io/en/v0.6.1/DeepSpeech.html}
\end{figure}


#### Beam search decoder
&#8204;\newline
A major challenge in converting speech to text is that words and letters can be pronounced at different speeds and do not occur sequentially in temporally equal segments that the neural network can process. This is a problem because everyday language is too unpredictable. To solve this problem DeepSpeech uses a Connectionist Temporal Classification (CTC) beam search decoder [@ctc_paper]. The RNN creates a transition matrix for each frame it iterates along the timeline, which determines how likely the frame is to contain the various symbols (letters & some special characters).

It would be easy to take the most probable symbol from each frame, but this would not be the most probable result. If a letter is pronounced longer, the RNN will recognize the same letter repeatedly. Example: "HHH\_\_EL\_LLL\_O".  
The blank symbol  "\_". is recognized by the RNN at optional letter boundaries, the blank (space) between words itself is a separate symbol.
This output is reduced to "hello" with the the so called. The most probable result is therefore not the sequence of the most probable symbols, but the most probable sequence of symbols with the same result after applying the reduction.

An example would be these 3 possible results of which not "Cat" but "Hat" is the most probable result:  
1. 45% probability for "C\_\_AA\_T"  
2. 30% probability for "HH\_A\_TT"  
3. 25% probability for "H\_\_AA\_T"  

Since the second and third result in the same string "HAT", alltogether it is more probable then "CAT".  

This reduction is performed through the following three steps.

1. predict a sequence of symbols.
2. repeating symbols are reduced to one if they are not separated by a blank ("\_").
3. blanks are removed.

Through these steps an example output of a RNN "HHH\_\_EL\_LLL\_O" is reduced to "Hello".

![](assets/images/ctc.png){width=90%}
\begin{figure}[!h]
\caption{CTC process}
\end{figure}


In the case of DeepSpeech, the acoustic model already includes the E2E model, which infers speech directly into letters and words. This can easily be used on its own to transcribe speech, but it is useful to include a language model as described in the following chapter \ref{lm_ds} to improve the output accuracy. Since DeepSpeech's acoustic model is already an E2E model, it directly outputs graphemes. A grapheme is the smallest semantically distinctive unit of a language. In the case of English this equals the alphabet a-z and some special characters.


#### Training of an acoustic model
&#8204;\newline
It is possible to train a custom acoustic model or to continue training an existing pre-trained model provided by Mozilla from a checkpoint. All that is needed is the client provided by Mozilla, implemented in Python, and a corpus with sufficient data. To train an acoustic model that can be used productively, however, amounts of data sets are required that are not yet publicly available. For example, Baidu has recorded about 10.000 hours of speech in a variety of situations for its DeepSpeech2 [@deepspeech_2] implementation. In addition to these 10.000 hours, another 100.000 hours of artificially created recordings were used. This was achieved by combining the 10.000 hours of original recordings with background noises of various kinds. For training with 1000 hours of audio alone, 20 hours are required with 4 standard graphics cards (e.g. NVIDIA GTX 1080 16gb). If this number is scaled to 100.000 hours of audio, it would take about 12 weeks for the fully trained model. 

Even with 8 NVIDIA Titan X GPUs, which according to Baidu is the maximum possible parallelism for training, it still takes two weeks to train an entire model with this amount of data [@deepspeech_2].

#### Pre-trained acoustic model \label{pretraineddsmodel}
&#8204;\newline
Mozilla provides pre-trained acoustic models that can be downloaded. These are versioned to be compatible with the different versions of DeepSpeech. Currently, the most recent model is available in version 0.6.0. When using this model, the version of this model must be compatible with the version of DeepSpeech. The 0.6.0 model was trained with the corpora LibriSpeech, Fisher, Switchboard and CommonVoice English. Altogether the corpora contain about 5000 hours of which 3816h were used as training material, 83% of which is american-english. The unused hours are either duplications or were used for development/testing purposes. Compared to the model from version 0.5.1 which was trained with only 1000 hours of LibriSpeech, the current model is based on a much larger amount of data. Accuracy has improved greatly, although foreign accents are still problematic, as they are only present with 17% in the training data. With the LibriSpeech Clean test, the model achieves a WER of 7.5%, which is a good result but it is to be noted that the LibriSpeech test set only consists of American English without background noise. With slow and accurate pronunciation it is still possible to achieve somewhat good results with a foreign accent. There is no empirical evidence for this as there is no test set which tests for accuracy with foreign accents. This would require a corpus which consists solely of foreign accents. With CommonVoice this would be possible in the future as the corpus stores the individual recordings with additional information about the speaker's origin.
In the following diagram the exact composition of the training data is summarized:


![](assets/images/pie_chart.png){width=65%}
\begin{figure}[!h]
\caption{Proportion of corpora used for the pre-trained acoustic model of DeepSpeech}
\end{figure}


### The language model of DeepSpeech \label{lm_ds}
Transcribing sounds to individual words is often not enough to form meaningful sentences. For instance, both the word "night" and "knight" sound exactly the same, which makes it impossible for the acoustic model to determine which of the two is spoken in a particular example. The task of the language model is to solve this problem by looking at how probably one word follows the other. 
In most cases these are uni- or n-gram models. Unigrams only contain single words - no word sequences - and indicate how likely they are to occur in the language. Thus the word "night" could occur more often than "knight" and would be prioritized. N-grams indicate for any number of consecutive words how likely these sequences are to occur. Thus, the Language model provides more context and understanding of a language for the result of the acoustic model, which without a language model would only output word sequences without context and understanding. An example would be the sentence "The night is dark". Without context, the acoustic model would not know whether it is "night" or "knight", but the language model estimates that it is more likely to be "night" than "knight".

#### Language model provided by Mozilla
&#8204;\newline
The language model provided by Mozilla consists of the most common words and sentences from the set of sentences/word sequences of the training material. Currently the language model of DeepSpeech is about 900 MB in size and consists of two files. The first one is the `trie`, a data structure used to search for strings and identify a specific word. During the transcription of a single word, the `trie` is used to decide whether the assumption of the acoustic model is correct or whether another similar sounding letter is more likely to occur in the sequence. For example, the letter "s" is more likely than a "z" to occur in the previously recognized letter "ha", since "has" occurs in the English language in contrast to "haz".

The second part is the language model itself, and in the case of DeepSpeech, it is created as a binary file called `lm.binary`. It contains 5-grams, which is a N-gram of the order five, which means that at most, word sequences of length five are given a probability. If sentences from the training material are shorter than 5 words, only the possible N-grams for the length of that sentence are calculated.

#### Creating a custom language model
&#8204;\newline
It is possible to create a custom language model. For example, if it consists of only 10 words, each output of the acoustic model will be mapped to one of these 10 words from the language model. The size of a language model is also dependent on the number of words it comprehends. For example, a language model in version 0.6.0 of DeepSpeech created with only 20 words has a size of only 3 KB and the corresponding `trie` only 2 KB. `trie` and `lm.binary` are created together in the same step, while the `trie` depends on the `lm.binary` and is created from it.


### Overview

The following figure shows the internal process of DeepSpeech.

![](assets/graphics/DeepSpeech_internal.png){width=100%}
\begin{figure}[!h]
\caption{Internal process for inference in DeepSpeech}
\end{figure}


### DeepSpeech in other programming languages
DeepSpeech is implemented in Python with TensorFlow. To be able to use it in other programming languages, language bindings are provided. Currently DeepSpeech supports the following environments:
- Python  
- Node.js  
- .NET Framework  

For Node.js, packages are provided via NPM, for .NET corresponding NuGet packages. In addition, there are native clients for Linux, MacOS and Windows, which can be executed locally on the device via command-line.

\pagebreak