# Analysis of suitable web technologies for DeepSpeech \label{analysis_web}
This chapter is aimed at working out which different web technologies can be used to integrate DeepSpeech into a modern web application. Starting from the web server, it will be examined which requirements have to be met for DeepSpeech in order to work out the necessary preceding components for the web server and client.


## Language choice for the backend
DeepSpeech is available in #C in .NET Framework, Python and Node.js. Since the API of DeepSpeech is the same everywhere, one has free choice when choosing the programming language for the web server.
Due to the current relevance and simplicity Node.js is a good choice, because it also shares the same language with the frontend and Node.js is known for its simplicity. 


## Requirements for DeepSpeech
The acoustic model of DeepSpeech is trained with files in 16kHz 16 bit mono `.wav` file format [@deepspeech_docs_official]. In practice, this means that the model can only transcribe data which is provided in exactly this format. 
At the moment this unfortunately is a weakness of neural networks the way they are currently designed.
The API of DeepSpeech offers two ways to transcribe audio: Fixed length audio files or from an audio stream.
Even if both use different functions of the API in the implementation, the correct format and input type must be given at both places which is a buffer ^[Part of a stream which is currently loaded for reading/writing] that contains the audio data.

## Choice of framework and browser API's for the frontend
The goal of this chapter is to find out which possibilities there are to obtain the audio data in the frontend. In concrete terms, two requirements for the frontend are derived from the possibilities with DeepSpeech:

1. Creating a continuous audio stream.
2. Recording a audio file which already is in the right format for DeepSpeech.

To meet these requirements, frameworks, browser API's and Javascript libraries are examined for their possibilities.


### Choice of the framework
Nowadays, hardly any web applications are created without a framework. The question is whether one of the frameworks is more suitable than another for integrating DeepSpeech. This question can easily be answered with a "no", because none of the big frameworks like Angular, React or Vue has special functionality that makes the handling with audio easier. The chapter is nevertheless included in this thesis to answer this question.
The frameworks only provide the basic architecture for a web application and use different design patterns to simplify the development of more complex applications. In contrast to libraries, they do not provide a specific implementation for handling audio streams for instance. 
A small advantage of Angular is the use of RxJS \footnote{Reactive Extensions Library for JavaScript} "[...] a library for reactive programming using Observables, to make it easier to compose asynchronous or callback-based code" [@rxjs_docs]. This makes it easier for example to handle the continuous transcriptions sent by the server when streaming continuous speech.

### Choice of browser API's
The goal in choosing the browser API's is to find out which API's allow to receive an audio stream from the user and send it to the web server in a format like base64 ^[base64 encoded data represens binary data in an ASCII string format] or as a BLOB ^[A Blob object represents a file-like object of immutable, raw data].

#### Media Capture and Streams API
&#8204;\newline
The Media Capture and Streams API - MediaStream API for short - is a browser API for accessing the user's video/audio hardware. 
With the user's consent, one has continuous access to a video/audio stream of the user. The format of the audio stream depends on the hardware but is often 44.100KHz 16 bit PCM and the samples themselves are stored in a Float32Array, one of the TypedArrays^[A TypedArray is an array-like object in javascript containing only specific types of elements like 16 bit integer values for an Int16Array.] of javascript which is very important for later processing.
A few browsers in the latest versions even allow specifying the sample rate at which the audio stream should be recorded.
The output of the media device, e.g. the microphone, can be configured to for example use its "NoiseSupression" or "EchoCancellation" function, so that the output stream contains less noise or echoes.
The API is available in all common browsers except Internet Explorer.

![](assets/images/mediastreamapi_CIU.png){width=100%}
\begin{figure}[!h]
\caption{Browser compatibility of MediaStream API }
\caption*{Source: https://caniuse.com/\#search=getusermedia }
\end{figure}

The MediaStream API is not to be confused with the outdated implementation of getUserMedia from the Navigator API.

#### MediaRecording API \label{mediaRecording_API}
&#8204;\newline
The MediaRecording API allows the processing of the stream received from the MediaStream APi. It offers the possibility to collect the data and make it available as a BLOB on demand[@mediarecordingapi].
This simplifies the handling of audio data, because the output generated by the MediaRecorder can be used directly.
The stream of the MediaStream API consists only of buffers in the format of Float32 arrays which contain the data of the recorded speech.
Since new data is added continuously and these tiny data snippets are not suitable for further processing, the MediaRecording API allows creating a BLOB from the buffer in defined intervals of e.g. 1000ms, which results in a valid audio recording. This is convenient because this way one can wait as long as needed and send the data collectively.
The MediaRecorder object sends an event containing the BLOB at the end of an interval. If no interval is specified, the event is sent as soon as the audio stream ends. This is useful for speech recognition if only one voice command is to be executed, which the user can record by holding down a button. 


![](assets/images/MediaRecorder_CIU.png){width=100%}
\begin{figure}[!h]
\caption{Browser compatibility of MediaRecorder API}
\caption*{Source: https://caniuse.com/\#search=MediaRecorder }
\end{figure}


The availability MediaRecording API is currently still limited, but the Javascript Library "Recorder.js" provides a suitable alternative, which also works in older browser versions.
Even if the use of third party libraries leads to unwanted dependencies, Recorder.js is still a good cross-browser compatible solution.
A special feature of Recorder.js is that the library allows to extract data directly in `wav` format.
Using both the MediaRecording API and the library Recorder.js requires further audiotranscoding  in the backend, since DeepSpeech needs the audio data with a sample rate of 16KHz [@deepspedech_baidu_paper].

#### Web Audio API - Client-side resampling \label{webAudioApi}
&#8204;\newline
Another approach eliminates the need to use the MediaRecording API and resamples the audio stream already in the frontend, which has decisive advantages. Resampling is the process of rewriting the audio stream from for example 44.100 Hz to 16.000 Hz, which is the format required by DeepSpeech.
Since one second only contains 16.000 samples instead of 44.100, the amount of data to be transferred to the web server is reduced. It also reduces the load on the web server enormously when many users send unformatted data to the server simultaneously. 
If DeepSpeech will be available in later versions in the browser on the client side, the Web Audio API is also the only way to encode the audio data correctly, as the server side components for encoding the data are no longer present.
Note that the audio source cannot be recorded with less than 16KHz, since resampling is only possible downwards.

For resampling the data must be processed in single sequences by a `ScriptProcessorNode` or `AudioWorkletNode`. They process the data chunk by chunk and output the resampled data into a new stream. This approach requires a custom implementation of a component, which maps the binary data chunks to a sample rate of 16KHz. The `ScriptProcessorNode` and `AudioWorkletNode` are two interfaces of the Web Audio API, the latter replacing the former.  
Although ScriptProcessorNode has been marked as obsolete, the replacement of `AudioWorkletNode` in the W3C standard is still in the "Editor's Draft", a design phase of W3C and therefore not yet recommended over the interface `ScriptProcessorNode`.

In order for this solution to output the resampled data collected in intervals, equivalent to the MediaRecorder API, further steps are necessary, which are explained in the practical part of this work in the chapter \ref{client_resampling}.


## Transmission of audio from client to server
From the previous chapters, the following requirements for the transfer of audio data from client to server can be concluded:  

1. Transfer of a single recording
2. Continuous streaming of audio data

This chapter will work out which communication protocols and methods are best suited for this purpose.

### WebRTC
WebRTC (Web Real-time Communication) provides communication protocols and programming interfaces for P2P (Peer-to-Peer) communication [@webrtc]. This allows two devices to communicate with each other without a web server/third party after the connection has been established.
A web server is required once to establish the connection. A classic use case for WebRTC are telephone calls or video chats between two or more users.
Even though WebRTC provides interfaces for audio and video transmission, it is not suitable for the use of DeepSpeech, because a communication between web server and client is required.
There are also server-side implementations where the server itself behaves like a client, but a traditional client-server architecture makes more sense here, because different clients want to communicate independently with the web server, not with each other, and want to use the function of the web server - the transcription of audio.


### XHR
XMLHttpRequest (XHR) is a browser interface for transferring data via HTTP^[HTTP functions as a request–response protocol for a client–server architecture]. If a connection to the Web server is open, data can be transferred or requested.
Virtually all modern HTTP wrappers such as axios, the FetchAPI or the HTTPClientModule from Angular use XHR internally and try to simplify handling of http requests. Each of these wrappers implements the interface somewhat differently, which is why the Angular HttpClientModule, for example, can only transfer raw data as an ArrayBuffer and not directly as an Int16Array or BLOB, although the underlying browser-native XHR interface allows this.

If the raw data is already available in its entirety, i.e. when the recording is fully captured, it makes sense to transfer it via HTTP. It is irrelevant whether it is actually a BLOB or the same data in raw format inside a Int16Array. 
Depending on the length of this recording, the individual parts of the BLOB/Int16Array are received asynchronously as buffers on the web server and can be further processed there.


### WebSocket
WebSockets provide a simple TCP-based transmission method that establishes a bi-directional connection between the web browser and the WebSocket server.
The origin and type of this data is not important. A Buffer or BLOB can also be transmitted without any problems.
This makes it perfect for continuous streaming of audio data to the web server in one direction, and the web server's response with the transcribed text in the other direction.

Libraries like Socket.IO simplify the development with the underlying technology of WebSockets and build on it with additional functionality.
For the simple use case of one client and one server the standard implementation of WebSockets are sufficient.
When streaming continuously from the browser, the MediaRecording API delivers a BLOB with the data collected up to that point every interval.
This BLOB can then be transmitted to the WebSocket server, which can write the data to the server-side stream for further processing


## Server side stream handling \label{evaluation_pipeline}

Once the audio data is received by the web server - whether as a BLOB or stream - it must be converted in several steps to a format that is readable by DeepSpeech.

To be able to process the audio data further, it must be modified via a stream pipeline. This is also the case when the audio is a finished recording, as large amounts of data should always be processed through a stream of individual buffers.

As described in previous chapters, there are two different use cases for this:

1. pipeline for continuous streaming
2. pipeline for converting an audio recording

Streams in Node.js have one input and one output. The input or the source of the stream pushes the data as a buffer into the stream and the modified output can be fed into the input of the next stream.
The source for audio streaming would be the WebSocket-Server which pushes the binary data from the audio stream of the frontend into the stream of the backend in intervals.
In Node.js there are different stream types like Readable, Writable or Duplex streams. For the WebSocket-server a readable stream is suitable, because buffers arriving via the Websocket-connection have to be pushed manually into the stream and the stream has to be readable by another stream. The naming "readable" means that the stream has to be able to be read out via a pipe from another (writable) stream. A pipe is structured as follows: `source.pipe(destination)`, whereas source must be readable and destination writable.
An example of a duplex stream would be `source.pipe(duplex).pipe(destination)`, since this is both source and destination.  
It is not to be misunderstood that a readable stream must still be manually pushed to, since it also needs a buffer that the destination can read from. It is just not able to be the "destination" of a pipeline.


This chapter will describe how such pipelines have to be built to be able to deliver the right format for DeepSpeech.


### FFMPEG \label{ffmpeg}
After the server-side stream was created via the WebSocket server and data is continuously loaded into the stream via WebSockets, the same stream that was created in the browser now exists on the web server.  
Since the format of the stream differs depending on the user's browser and hardware/operating system, the first step in the pipeline is to rewrite the stream to a uniform format readable by DeepSpeech.
The only format readable by DeepSpeech is `16le PCM` in raw format or in a `wav` file container, which is a signal with one channel (mono), 16bit bit depth and 16KHz sample rate.  
A suitable software component for this is FFMPEG, a program library which can convert audio and video formats.
This can be used in Node.js by starting FFMPEG as a child process on which various commands can be executed. For this purpose FFMPEG must be installed on the operating system of the web server.
As described in chapter \ref{webAudioApi}, other approaches exist where the stream is already converted to the appropriate format in the frontend, but the server-side approach is also a practical solution depending on the use case.
FFMPEG has an interface for converting a stream as well as one for static/completed recordings.


### VAD
One of the big challenges in continuously transcribing the audio stream is to recognize the difference between silence and speech, so that individual sentences/inputs can be recognized and the stream can be processed step by step only when speech is spoken. 
This is where Voice activity detection (VAD) comes in.  
A robustly trained acoustic model should be able to handle silence correctly, but it makes sense to load the neural network only when speech is actually being spoken. 
If VAD is used in the stream pipeline, it will only pass on data containing words in its output stream.
For Node-js there is the NPM package `node-vad`. Depending on the configuration of the VAD stream, the activity detection reacts more sensitive.
A very important note is that `node-vad` in the current version has problems with the sensitivity, which causes a few milliseconds to be missing at the beginning and end of the speech.
This causes the first word to not being recognized by DeepSpeech or being recognized incorrectly when it is short.
In this pipe VAD receives the formatted stream from FFMPEG and passes the buffers (a chunk of the stream) containing speech to DeepSpeech.

VAD itself only accepts streams with a sample rate of 8, 16, 32 or 48 KHz. If this was not the case, and VAD would support any format, it would make more sense to use VAD in the pipeline before FFMPEG, because then resampling does not have to be done for the whole continuous stream, but only for those parts where speech is spoken, which would save resources.


### DeepSpeech streaming

In DeepSpeech there are basically two ways to obtain a transcription.

1. start inference after all of the data is available
2. stream data and already transcribe while speaking.

The former would result in waiting too long to get a response if the recording is very long. Therefore the streaming interface offers the possibility to start the transcription while the recording is still ongoing. Transcriptions can be requested while the stream is active as well as when it is finished. Even if the client recording is already available in its entirety and is sent via XHR, the data would still arrive on the web server in chunks, depending on the length of the recording, as is the case with the WebSocket connection. Therefore, it would be a possibility to use the streaming interface of DeepSpeech instead of awaiting the complete data.

### SOX
Chapter \ref{ffmpeg} showed that FFMPEG is suitable for resampling an audio stream.
However, if the frontend already is delivering the audio binary data in wav format as a BLOB to the backend, i.e. when an audio recording is sent from Recorder.js, only the sampling rate and bit depth needs to be adjusted.
It would be possible to use FFMPEG for this, but it is easier to do this with the library SOX if the data is already in `wav` format and not in `audio/webm` format which would be the case if the blob was created using the MediaRecording API.
Similar to FFMPEG, SOX is a program library for converting audio. But unlike FFMPEG, it does not require a child process to be started in the Node.js runtime environment. However it cannot handle data from the `audio/webm` file container.

Sox provides several Node.js packages, including `sox-stream` which can be used to convert streams.
Even if the audio recording is finite and already exists as a finished recording, the audio file must be streamed again on the server side, as this simplifies the handling of the audio data and otherwise converting longer files would synchronously block the event loop of Node.js.

## General summary

In previous chapters, individual components were worked out. How these work together in general is to be shown in this chapter.

The following figure shows the roundtrip for both cases - transcribing a continuous stream and an audio recording - in case the audio data is sent unformatted to the web server.  

![](assets/graphics/Architektur_web.png){width=100%}
\begin{figure}[!h]
\caption{Web architecture roundtrip with server side resampling}
\label{roundtripWebArchitecture}
\end{figure}

To send the completed audio recording, it would also be useful to use WebSockets, but since this is actually only one "request", XHR makes more sense, since no ongoing connection is needed after sending the recording and you would have to manually closed.
If the audio data is already resampled in the frontend, the server-side components responsible for resampling are not needed.

![](assets/graphics/Architektur_web_resampling.png){width=100%}
\begin{figure}[!h]
\caption{Web architecture roundtrip with client side resampling}
\label{roundtripWebArchitectureResampling}
\end{figure}

If it is already possible to specify in the MediaStreams API at which sample rate a recording shall be made, neither the Web Audio API nor any server-side transcoding is needed. However, this is not a very common feature and only available in a few modern browser versions.

The decision whether to resample client- or server side depends on the circumstances. If an increased network traffic is not a problem, it is sufficient to resample the data on the web server first to reduce the processing power of the user's device. 



\pagebreak