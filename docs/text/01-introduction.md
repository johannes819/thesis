# Introduction 

## Motivation and relevance

In recent years the world of software development has been revolving more and more around the Web. Requirements for web applications are increasing, which is why new solutions are constantly being developed.
Especially at times when voice assistants are gaining more and more ground through smart homes, the desire to integrate voice control into one's own web application is also increasing. So far, this has mainly been possible via external API's from large companies like Google or with more complex open source systems. For privacy and security reasons this is not an option for all projects and more complex open source systems require more powerful server side hardware and maintanance. Hence, a simple open source solution was needed. Recently, a Speech-to-Text (STT) engine^[An API based software component capable of transcribing speech into text] developed by Mozilla based on an architecture introduced by Baidu^[One of the largest AI and internet companies in the world based in china] called "DeepSpeech" has been released, which can be used by everyone for free. If and how this new technology can be used for the web and which new possibilities it offers will be examined in this thesis.


## Objective

The goal of this thesis was to allow the user to convert speech to text in real time in a demo application. Only freely accessible software components may be used, so that the developer has full control over the entire process - from recording the speech via the user's microphone to transcribing the speech via a neural network^[An artificial intelligence component which is capable of making a prediction based on some kind of input]. The main challenge is to identify the most suitable components and to make the communication between them as efficient as possible.

In particular, the following questions are discussed in this thesis:

- How does DeepSpeech work?
- Where does the data come from that is used for the training of the STT-engine?
- Which software components can be used to integrate DeepSpeech into a web application?
- In what different ways can DeepSpeech be used?


## Structure

Chapter \ref{analysis_stt} of this paper will first analyze how DeepSpeech compares to other STT engines and what requirements need to be met.
It also explains the exact architecture and components of DeepSpeech, how they work together and how it differs from traditional solutions.

After the basics to understand DeepSpeech are covered, chapter \ref{analysis_web} analyzes how DeepSpeech interacts with suitable web technologies and how it can be integrated into a web application.

In the last - the practical - part of this thesis the chosen technologies will be used and implemented in a demo application so that at the end a prototype is available which is ready to convert speech input via the user's microphone from the browser to text via the self-implemented web server with the DeepSpeech.

\pagebreak