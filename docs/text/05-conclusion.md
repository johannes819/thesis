
# Conclusion \label{conclusion}

The DeepSpeech architecture presented by Baidu is a state-of-the-art e2e speech recognition system. Years of consecutive research have made it possible to solve this complex problem with a very simple architecture with few components. For a developer this means that he can integrate ASR into a web application in the simplest possible way. Only a few steps are necessary because Mozilla provides all necessary components. 

Unfortunately, Mozilla's implementation DeepSpeech is still rather unsuitable for professional use or only recommended for some special cases. Due to the problems mentioned in chapter \ref{pretraineddsmodel}, DeepSpeech scores very low on background noise and accents. 
 
However, the accuracy is greatly improved with a custom language model consisting of only a couple of words. When creating the language model, the accuracy is improved even further by adding common combinations of words. For example, for ordering any number of "gin tonic", adding the combinations "one gin tonic, two gin tonic, three gin tonic..." to the language model would be helpful because the language model then knows what other words can occur in combination with the word "gin tonic".

If one were to compare DeepSpeech with voice assistants like Alexa or Google Assistant, one would not think that there is a competitive architecture behind it. The reason for the success of Alexa or Google Assistant being the amounts of data these companies own. If the same amount of data were available open-source, DeepSpeech would not be far from the accuracy of these assistants. The key for the success of DeepSpeech is primarily CommonVoice and a continuous development and improvement of the underlying architecture.

The advantage of the solutions developed in this thesis is that only the DeepSpeech acoustic model needs to be replaced to improve the overall result. Since the API should not change too much, but only the underlying implementation, the processes presented in this thesis are reusable for future versions of DeepSpeech.
To improve the accuracy, all that is needed is a `npm update deepspeech` and the replacement of the acoustic model.

It always depends on the individual case, but one of the most elegant solutions would be the combination of client-side resampling and utilizing the streaming interface of DeepSpeech. This combination minimizes the data transfer to the server and the response time of DeepSpeech. 

At this point in time, there is still a lot of additional knowledge needed about DeepSpeech, as the implementation does not yet abstract some very technical details with its API. For future releases Mozilla has announced an improvement regarding this issue.

Overall, this very bleeding edge technology will be a great asset to the open-source community and for developers who need a simple, yet effective STT solution. There is yet a lot to improve but with the right time given, we could be close to having this powerful tool at hand for our own projects.


\pagebreak