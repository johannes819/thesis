---
title: "Analysis and implementation of a web-based Speech-to-Text system with DeepSpeech using freely available software components"
subtitle: |
    | Bachelor Thesis WS2019/2020
    | im Studiengang 
    | Medieninformatik
    | Hochschule der Medien Stuttgart
    |
    | Erstprüfer - Professor Dr. Fridtjof Toennissen
    | Zweitprüfer - Frederik von Berg
author: Vorgelegt von Johannes Beiser, Matrikel-Nr. 33570
date: "12.02.2020"
subject: "Bachelor Thesis WS2019/2020"

titlepage: true
titlepage-rule-height: 2
toc-own-page: true
linestretch: 1.5
listings-no-page-break: true

---

\listoffigures

\listoftables
\pagebreak
# List of abbreviations {-}
AI - Artificial intelligence  
STT - Speech-to-Text  
ASR - Automatic Speech Recognition  
SNR - Signal to noise ratio  
ANN - Artificial neural network  
RNN - Recurrent neural Network  
E2E - End-to-End  
CTC - Connectionist Temporal Classification  
WER - Word Error Rate  
BLOB - Binary Large Object  
XHR - XMLHttpRequest  
P2P - peer-to-peer  
VAD - Voice activity detection  
PCM -  Pulse code modulation  
EOF - End of File  
MFCC - Mel Frequency Cepstral Coefficients  

\pagebreak
